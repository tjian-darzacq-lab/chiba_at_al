%Code to detect and quantify spots from a single image plane for large
%images, such as those from the StellarVision microscope
% David McSwiggen, September 2016
clear; clc; close all;

% The puropse of this code is to detect spots in DNA or RNA FISH images
% using the MTT fitting algorithm as the basis for detection. Two image
% files -- in this case Telomere and Centromere -- are analyzed separately
% with one acting as an internal control for the other. A third image file
% of a DAPI image or similar, is used to identify cell nuclei and to ensure
% that detections are bona fide spots rather than spurrious detections
% stemming from the SAO reconstruction.

% A user should specify an input path where the image files are held in the
% 'input_path', and identify the names of individual files, and then
% specify where the processed files should be saved.

%This code allows for the user to identify regions of the image containing
%different cell types (Identified here as Nevus, 

%% Image Parameters
%%%%%%%%%%%%%%%%%%%% DEFINE INPUT AND OUTPUT PATHS %%%%%%%%%%%%%%%%%%%%%%%%
% specify input path with tiff files:
input_path=('/media/davidm/Elements/OBI/matfiles_analysis/');
DAPI_file = 'Case4_2_nevus_7x7_Dapi.mat';
Experiment_condition_file=('Case4_2_nevus_7x7_Cy3.mat');
Internal_Control_file=('Case4_2_nevus_7x7_Cy5.mat');
output_filename = 'Test';
output_path=('/media/davidm/Elements/OBI/matfiles_analysis/'); %Give filepath to EXISTING folder where you want to save the results
% Folder_to_annotate = 'tiff_images_to_annotate'; %Matlab will make this folder
% Directory_to_localize = 'stacks_to_localize'; %Matlab will make this folder

% add the required functions to the path. These functions are required to
% open the image files as well as to perform the MTT localization
% algorithm.
addpath('/data2/David_M/Matlab/SLIMFAST_batch_fordist');
addpath('/data2/David_M/Matlab/SLIMFAST_batch_fordist/bfmatlab');
disp('added paths for MTT algorithm mechanics, bioformats...');

%Set Thresholds
mfile = 1;
DAPI_threshold_value = 1.1; %DAPI intensity considered IN a nucleus 
number_nearest_neighbor = 5; %Number of neighboring spots in control channel to use during normalization.
field_detect_side_size = 512; %in pixels
Detection_threshold = 375; %Spots below this threshold are discarded
LocalizationError_Experiment = -3; % Localization Error: -6 = 10^-6
LocalizationError_Normalization = -3; % Localization Error: -6 = 10^-6
EmissionWavelength_Expermient = 800; % wavelength in nm; consider emission max and filter cutoff
EmissionWavelength_Control = 800; % wavelength in nm; consider emission max and filter cutoff
NumDeflationLoops = 0; % Generaly keep this to 0;

%Which Operations will you perform?
Stellarvision = 1;
Save_output = 1;
Localize_Experiment = 1;
Localize_Control = 1;

[s,mess,messid] = mkdir(output_path, output_filename);

%% READ IN RAW IMAGES
if mfile == 1 %large .mat files, if the analysis has already been run before.
    image_dapi_struct = load([input_path, DAPI_file]);
    image_experiment_struct = load([input_path,Experiment_condition_file]);
    image_control_struct = load([input_path,Internal_Control_file]);
    original_image_dapi = imresize(double(image_dapi_struct.M),4);
    original_image_experiment = double(image_experiment_struct.M);
    original_image_control = double(image_control_struct.M);
    Image_x_size = size(original_image_dapi,2);

else % If loading a .tiff file
    image_dapi_struct = tiffread([input_path, DAPI_file]); %Should be 16384 X 16384 px
    image_experiment_struct = tiffread([input_path,Experiment_condition_file]);
    image_control_struct = tiffread([input_path,Internal_Control_file]);
    Image_x_size = image_dapi_struct.width;
    original_image_dapi = double(image_dapi_struct.data);
    original_image_experiment = (double(image_experiment_struct.data));
    original_image_control = double(image_control_struct.data);
end


tic;
if Stellarvision == 1
    %Generate downsampled image for masking
    image_downsample_ratio = 2048/Image_x_size;
    mask_upsample_ratio = Image_x_size/2048;
else
    image_downsample_ratio = 1;
    mask_upsample_ratio = 1;
end

dwnsmpl_image_dapi = imresize(original_image_dapi,image_downsample_ratio);
dwnsmpl_image_experiment = imresize(original_image_experiment,image_downsample_ratio);
dwnsmpl_image_control = imresize(original_image_control,image_downsample_ratio);



figure('position',[20 100 800 1200]);
hold on;
imshow(dwnsmpl_image_dapi,[]);
impixelinfo;
toc;

message = sprintf('Detemine how many polygons you will need to annotate\neach tissue type, then click OK');
uiwait(msgbox(message));
close all;
clear 'original_image_experiment' 'original_image_control'

%% GENERATE IMAGE MASKS
close;
seg_prompt = {'How many Nevi?','How many Melanoma regions?', 'How many Lymphocyte regions?','How many Fibroblast regions?'};
seg_prompt_name = 'Wait for user';
numlines = 1;
seg_defaults = {'1','1','1','1'};
options.Resize = 'on';
options.WindowStyle = 'normal';
seg_answer = cell2mat(inputdlg(seg_prompt,seg_prompt_name,numlines,seg_defaults));

close;

Nevus_region_answer = str2num(seg_answer(1,:));
Nevus_individual_masks = zeros(size(dwnsmpl_image_dapi,1),size(dwnsmpl_image_dapi,2),Nevus_region_answer);
Nevus_masks_all = zeros(size(dwnsmpl_image_dapi,1),size(dwnsmpl_image_dapi,2));

Melanocyte_region_answer = str2num(seg_answer(2,:));
Melanocyte_individual_masks = zeros(size(dwnsmpl_image_dapi,1),size(dwnsmpl_image_dapi,2),Melanocyte_region_answer);
Melanocyte_masks_all = zeros(size(dwnsmpl_image_dapi,1),size(dwnsmpl_image_dapi,2));

Lymphocyte_region_answer = str2num(seg_answer(3,:));
Lymphocyte_individual_masks = zeros(size(dwnsmpl_image_dapi,1),size(dwnsmpl_image_dapi,2),Lymphocyte_region_answer);
Lymphocyte_masks_all = zeros(size(dwnsmpl_image_dapi,1),size(dwnsmpl_image_dapi,2));

Fibroblast_region_answer = str2num(seg_answer(4,:));
Fibroblast_individual_masks = zeros(size(dwnsmpl_image_dapi,1),size(dwnsmpl_image_dapi,2),Fibroblast_region_answer);
Fibroblast_masks_all = zeros(size(dwnsmpl_image_dapi,1),size(dwnsmpl_image_dapi,2));



%%%%% CELL TYPE MASKS %%%%%%%%
iter = 1;
message = sprintf('Draw ROIs around Nevi regions');
uiwait(msgbox(message));
for region = 1:Nevus_region_answer
    Nevus_masks_all(:,:) = sum(Nevus_individual_masks,3)>0;
    Nevus_masks_all(:,:) = imfill(Nevus_masks_all(:,:),'holes');
    Nevus_outlines = bwboundaries(Nevus_masks_all(:,:),8);
    numberOf_Nevus_Boundaries = size(Nevus_outlines, 1);
    
    hold on;
    imshow(dwnsmpl_image_dapi,[]);
    axis image;
    impixelinfo;
    for point = 1 : numberOf_Nevus_Boundaries
        KerBoundary = Nevus_outlines{point};
        plot(KerBoundary(:,2), KerBoundary(:,1), 'g', 'LineWidth', 3);
    end
    
    hold off;
    [Nevus_individual_masks(:,:,region), x_coord_ker, y_coord_ker] = roipoly();
    if region == Nevus_region_answer
        close;
    else
        message = sprintf('Outline another region');
        uiwait(msgbox(message));
    end
    iter = iter+1;
end

iter = 1;
message = sprintf('Draw ROIs around Melanocyte-dense regions');
uiwait(msgbox(message));
for region = 1:Melanocyte_region_answer
    Melanocyte_masks_all(:,:) = sum(Melanocyte_individual_masks,3)>0;
    Melanocyte_masks_all(:,:) = imfill(Melanocyte_masks_all(:,:),'holes');
    Melanocyte_outlines = bwboundaries(Melanocyte_masks_all(:,:),8);
    numberOf_Melanocyte_Boundaries = size(Melanocyte_outlines, 1);
    
    hold on;
    imshow(dwnsmpl_image_dapi,[]);
    axis image;
    impixelinfo;
    for point = 1 : numberOf_Melanocyte_Boundaries
        MelBoundary = Melanocyte_outlines{point};
        plot(MelBoundary(:,2), MelBoundary(:,1), 'r', 'LineWidth', 3);
    end
    hold off;
    [Melanocyte_individual_masks(:,:,region), x_coord_Mel, y_coord_Mel] = roipoly();
    if region == Melanocyte_region_answer
        close;
    else
        message = sprintf('Outline another region');
        uiwait(msgbox(message));
    end
    iter = iter+1;
end

iter = 1;
message = sprintf('Draw ROIs around Lymphocyte-dense regions');
uiwait(msgbox(message));
for region = 1:Lymphocyte_region_answer
    Lymphocyte_masks_all(:,:) = sum(Lymphocyte_individual_masks,3)>0;
    Lymphocyte_masks_all(:,:) = imfill(Lymphocyte_masks_all(:,:),'holes');
    Lymphocyte_outlines = bwboundaries(Lymphocyte_masks_all(:,:),8);
    numberOf_Lymphocyte_Boundaries = size(Lymphocyte_outlines, 1);
    
    hold on;
    imshow(dwnsmpl_image_dapi,[]);
    axis image;
    impixelinfo;
    for point = 1 : numberOf_Lymphocyte_Boundaries
        LymphBoundary = Lymphocyte_outlines{point};
        plot(LymphBoundary(:,2), LymphBoundary(:,1), 'b', 'LineWidth', 3);
    end
    
    hold off;
    [Lymphocyte_individual_masks(:,:,region), x_coord_Lymph, y_coord_Lymph] = roipoly();
    if region == Lymphocyte_region_answer
        close;
    else
        message = sprintf('Outline another region');
        uiwait(msgbox(message));
    end
    iter = iter+1;
end

iter = 1;
message = sprintf('Draw ROIs around Fibroblast-dense regions');
uiwait(msgbox(message));
for region = 1:Fibroblast_region_answer
    Fibroblast_masks_all(:,:) = sum(Fibroblast_individual_masks,3)>0;
    Fibroblast_masks_all(:,:) = imfill(Fibroblast_masks_all(:,:),'holes');
    Fibroblast_outlines = bwboundaries(Fibroblast_masks_all(:,:),8);
    numberOf_Fibroblast_Boundaries = size(Fibroblast_outlines, 1);
    
    hold on;
    imshow(dwnsmpl_image_dapi,[]);
    axis image;
    impixelinfo;
    for point = 1 : numberOf_Fibroblast_Boundaries
        FibBoundary = Fibroblast_outlines{point};
        plot(FibBoundary(:,2), FibBoundary(:,1), 'w', 'LineWidth', 3);
    end
    hold off;
    [Fibroblast_individual_masks(:,:,region), x_coord_Fib, y_coord_Fib] = roipoly();
    if region == Fibroblast_region_answer
        close;
    else
        message = sprintf('Outline another region');
        uiwait(msgbox(message));
    end
    iter = iter+1;
end

%%%%% Generate Binary Masks and Outlines for Each Cell Type %%%%%
Nevus_masks_all(:,:) = sum(Nevus_individual_masks,3)>0;
Nevus_masks_all(:,:) = imfill(Nevus_masks_all(:,:),'holes');
Nevus_masks_all_display = Nevus_masks_all;
Nevus_masks_all = imresize(Nevus_masks_all, mask_upsample_ratio);
Nevus_outlines = bwboundaries(Nevus_masks_all_display,8);
numberOf_Nevus_Boundaries = size(Nevus_outlines, 1);

Melanocyte_masks_all(:,:) = sum(Melanocyte_individual_masks,3)>0;
Melanocyte_masks_all(:,:) = imfill(Melanocyte_masks_all(:,:),'holes');
Melanocyte_masks_all_display = Melanocyte_masks_all;
Melanocyte_masks_all = imresize(Melanocyte_masks_all, mask_upsample_ratio);
Melanocyte_outlines = bwboundaries(Melanocyte_masks_all_display,8);
numberOf_Melanocyte_Boundaries = size(Melanocyte_outlines, 1);

Lymphocyte_masks_all(:,:) = sum(Lymphocyte_individual_masks,3)>0;
Lymphocyte_masks_all(:,:) = imfill(Lymphocyte_masks_all(:,:),'holes');
Lymphocyte_masks_all_display = Lymphocyte_masks_all;
Lymphocyte_masks_all = imresize(Lymphocyte_masks_all, mask_upsample_ratio);
Lymphocyte_outlines = bwboundaries(Lymphocyte_masks_all_display,8);
numberOf_Lymphocyte_Boundaries = size(Lymphocyte_outlines, 1);

Fibroblast_masks_all(:,:) = sum(Fibroblast_individual_masks,3)>0;
Fibroblast_masks_all(:,:) = imfill(Fibroblast_masks_all(:,:),'holes');
Fibroblast_masks_all_display = Fibroblast_masks_all;
Fibroblast_masks_all = imresize(Fibroblast_masks_all, mask_upsample_ratio);
Fibroblast_outlines = bwboundaries(Fibroblast_masks_all_display,8);
numberOf_Fibroblast_Boundaries = size(Fibroblast_outlines, 1);

All_masks = (Nevus_masks_all + Melanocyte_masks_all + Lymphocyte_masks_all + Fibroblast_masks_all);


%%%%Display image with annotations
figure('position',[20 100 800 800]);
imshow(dwnsmpl_image_dapi,[]);
title('Outlines superimposed');
axis image;
hold on;
for point = 1 : numberOf_Nevus_Boundaries
    KerBoundary = Nevus_outlines{point};
    plot(KerBoundary(:,2), KerBoundary(:,1), 'g', 'LineWidth', 3);
end

for point = 1 : numberOf_Melanocyte_Boundaries
    MelBoundary = Melanocyte_outlines{point};
    plot(MelBoundary(:,2), MelBoundary(:,1), 'r', 'LineWidth', 3);
end

for point = 1 : numberOf_Lymphocyte_Boundaries
    LymphBoundary = Lymphocyte_outlines{point};
    plot(LymphBoundary(:,2), LymphBoundary(:,1), 'b', 'LineWidth', 3);
end

for point = 1 : numberOf_Fibroblast_Boundaries
    FibBoundary = Fibroblast_outlines{point};
    plot(FibBoundary(:,2), FibBoundary(:,1), 'w', 'LineWidth', 3);
end
hold off;

message = sprintf('If these annotations look correct, hit OK. \nOtherwise abort the script and begin again.');
uiwait(msgbox(message));
    

%% GENERATE IMAGE STACK FOR LOCALIZATION
if Stellarvision == 1
    disp('Subdividing image...');
    % In this part, should read in one large image (Stellarvision) and
    % divide into 1024px X 1024px squares, based on fact that Stellarvision
    % images are 16384x16384px ((1024*16) X (1024*16)). Generate a false
    % "stack" for localization. The same parameters should be applied to
    % all channels so they can be referenced later.
    %If small images are aquired, they should be pre-formatted into a TIFF
    %stack, with the DAPI and FISH channels in the same order for masking
    %later on.
    Number_of_rows = Image_x_size/field_detect_side_size;
    Number_of_columns = Image_x_size/field_detect_side_size;
    Number_subsquares = Number_of_columns*Number_of_rows;
    dapi_binary_stack = zeros(field_detect_side_size,field_detect_side_size, Number_subsquares);
    subsquare_reference_matrix = zeros(Number_subsquares,3);
    tic;
    for square= 1:Number_subsquares
        % Row1: 1  2  3  4
        % Row2: 5  6  7  8
        % Row3: 9 10 11 12
        which_row = ceil(square/Number_of_columns); %First find which row the square belongs to
        which_column = square-((which_row-1)*Number_of_columns); %Then, use the information about the row it is in to find which column the square is in
        subsquare_reference_matrix(square,1) = which_row; %Save the the info about where the subsquare came. Column 1: Row in original image, Column 2: Column in original image
        subsquare_reference_matrix(square,2) = which_column;
        
    end
    toc;
    
    
    tic;
    disp('Gaussian filtering selected reagions');
    for square= 1:Number_subsquares
        % Row1: 1  2  3  4
        % Row2: 5  6  7  8
        % Row3: 9 10 11 12
        disp(['region ', num2str(square), ' of', num2str(Number_subsquares)]);
        which_row = ceil(square/Number_of_columns); %First find which row the square belongs to
        which_column = square-((which_row-1)*Number_of_columns); %Then, use the information about the row it is in to find which column the square is in
        x_start = 1 + (which_column-1)*field_detect_side_size;
        x_end = which_column*field_detect_side_size;
        y_start = 1 + (which_row-1)*field_detect_side_size;
        y_end = which_row*field_detect_side_size;
        if max(max(All_masks(y_start:y_end,x_start:x_end))) > 0
            gauss_dapi_image_stack = imgaussfilt(original_image_dapi(y_start:y_end,x_start:x_end),8)./imgaussfilt(original_image_dapi(y_start:y_end,x_start:x_end),50);
            dapi_binary_stack(:,:,square) = and((gauss_dapi_image_stack > DAPI_threshold_value),...
                (Nevus_masks_all(y_start:y_end,x_start:x_end)+Melanocyte_masks_all(y_start:y_end,x_start:x_end)+Fibroblast_masks_all(y_start:y_end,x_start:x_end)+Lymphocyte_masks_all(y_start:y_end,x_start:x_end)));
        else
            dapi_binary_stack(:,:,square) = zeros(field_detect_side_size,field_detect_side_size);
        end
    end
    toc;
    
    disp('Generating single DAPI mask');
    dapi_binary = zeros(Image_x_size,Image_x_size);
    tic;
    for square = 1:Number_subsquares
        y_start = 1+(subsquare_reference_matrix(square,1)-1)*field_detect_side_size;
        y_end = field_detect_side_size + (subsquare_reference_matrix(square,1)-1)*field_detect_side_size;
        x_start = 1+(subsquare_reference_matrix(square,2)-1)*field_detect_side_size;
        x_end = field_detect_side_size + (subsquare_reference_matrix(square,2)-1)*field_detect_side_size;
        dapi_binary(y_start:y_end,x_start:x_end) = dapi_binary_stack(:,:,square);
    end
    toc;
    clear 'original_image_dapi' 'dapi_binary_stack'
    
    tic;
    if mfile == 1 %large .mat file
        image_experiment_struct = load([input_path,Experiment_condition_file]);
        original_image_experiment = double(image_experiment_struct.M);
        Image_x_size = size(original_image_experiment,2);
        
    else % If loading a .tiff file
        image_experiment_struct = tiffread([input_path,Experiment_condition_file]);
        Image_x_size = image_experiment_struct.width;
        original_image_experiment = (double(image_experiment_struct.data));
    end
    disp('Separating experimental dataset into stacks');
    Experiment_meta_stack = zeros(field_detect_side_size,field_detect_side_size,Number_subsquares);
    for square = 1:Number_subsquares
        disp(['region ', num2str(square), ' of', num2str(Number_subsquares)]);
        which_row = ceil(square/Number_of_columns); %First find which row the square belongs to
        which_column = square-((which_row-1)*Number_of_columns); %Then, use the information about the row it is in to find which column the square is in
        x_start = 1 + (which_column-1)*field_detect_side_size;
        x_end = which_column*field_detect_side_size;
        y_start = 1 + (which_row-1)*field_detect_side_size;
        y_end = which_row*field_detect_side_size;
        if max(max(dapi_binary(y_start:y_end,x_start:x_end)))>0
            subsquare_reference_matrix(square, 3) = 1;
            Experiment_meta_stack(:,:,square) = original_image_experiment(y_start:y_end,x_start:x_end);
        else
            subsquare_reference_matrix(square, 3) = 0;
            Experiment_meta_stack(:,:,square) = zeros(field_detect_side_size,field_detect_side_size);
        end
    end
    clear 'original_image_experiment'
    toc;
    
    tic;
    if mfile == 1 %large .mat file
        image_control_struct = load([input_path,Experiment_condition_file]);
        original_image_control = double(image_control_struct.M);
        Image_x_size = size(original_image_control,2);
        
    else % If loading a .tiff file
        image_control_struct = tiffread([input_path,Experiment_condition_file]);
        Image_x_size = image_control_struct.width;
        original_image_control = (double(image_control_struct.data));
    end
    disp('Separating normalization dataset into stacks');
    Normalization_meta_stack = zeros(field_detect_side_size,field_detect_side_size,Number_subsquares);
    for square = 1:Number_subsquares
        disp(['region ', num2str(square), ' of', num2str(Number_subsquares)]);
        which_row = ceil(square/Number_of_columns); %First find which row the square belongs to
        which_column = square-((which_row-1)*Number_of_columns); %Then, use the information about the row it is in to find which column the square is in
        x_start = 1 + (which_column-1)*field_detect_side_size;
        x_end = which_column*field_detect_side_size;
        y_start = 1 + (which_row-1)*field_detect_side_size;
        y_end = which_row*field_detect_side_size;
        if max(max(dapi_binary(y_start:y_end,x_start:x_end)))>0
            Normalization_meta_stack(:,:,square) = original_image_control(y_start:y_end,x_start:x_end);
        else
            Normalization_meta_stack(:,:,square) = zeros(field_detect_side_size,field_detect_side_size);
        end
    end
    clear 'original_image_control'
    toc;
    
    if Save_output == 1
        save([output_path,output_filename,'/','Image_stacks'],'dapi_binary','Experiment_meta_stack','Normalization_meta_stack','subsquare_reference_matrix');
    end
    
else
    Experiment_meta_stack(:,:) = original_image_experiment;
    Normalization_meta_stack(:,:) = original_image_control;
    
    if Save_output == 1
        save([output_path,output_filename,'/','masks'],'dapi_binary','Experiment_meta_stack','Normalization_meta_stack','subsquare_reference_matrix');
    end
    
end

%% PARTICLE LOCALIZATION

%In this section, the code built in SLIMfast should be extracted, and
%all the necessary parameters (aquisition and localization) set so
%that the script can run a localization using MTT algorithm on the
%false stack of images.

%%% DEFINE STRUCTURED ARRAY WITH ALL THE MTT SETTINGS FOR LOCALIZATION %%%




%INPUT: (dir_path,impars, locpars, imstack) => Directory info, Image (aquisition) parameters, localization parameters, Filepate to tiff stack
if Localize_Experiment == 1
    tic;
    % imaging parameters
impars.PixelSize=0.07; % um per pixel
impars.psf_scale=1.35; % PSF scaling
impars.NA=1.49; % NA of detection objective
impars.FrameRate= 1000; %secs %Not sure if this is actually in important input for localization, but don't want to mess with it. Nonesense number
impars.FrameSize= 1000; %secs

% localization parameters
locpars.wn=9; %detection box in pixels
locpars.errorRate= LocalizationError_Experiment; % error rate (10^-)
locpars.dfltnLoops= NumDeflationLoops; % number of deflation loops
locpars.minInt=0; %minimum intensity in counts
locpars.maxOptimIter= 50; % max number of iterations
locpars.termTol= -2; % termination tolerance
locpars.isRadiusTol=false; % use radius tolerance
locpars.radiusTol=50; % radius tolerance in percent
locpars.posTol= 1.5;%max position refinement
locpars.optim = [locpars.maxOptimIter,locpars.termTol,locpars.isRadiusTol,locpars.radiusTol,locpars.posTol];
locpars.isThreshLocPrec = false;
locpars.minLoc = 0;
locpars.maxLoc = inf;
locpars.isThreshSNR = false;
locpars.minSNR = 0;
locpars.maxSNR = inf;
locpars.isThreshDensity = false;

    disp('Localizing particles in experimental channel');
    impars.name = Experiment_condition_file;
    impars.wvlnth= EmissionWavelength_Expermient/1000; %emission wavelength in um
    impars.psfStd= impars.psf_scale*0.55*(impars.wvlnth)/impars.NA/1.17/impars.PixelSize/2; % PSF standard deviation in pixels
    Localizations_raw_experiment = localizeParticles_ASH_par(input_path,impars, locpars, Experiment_meta_stack);
    %data structured array: % ctrsX = x-coord of all localizations
    % ctrsY = y-coord of all localizations
    % ctrsN = number of localizations in a given
    % frame
    % signal = signal (obvious)
    % frame (In this case, subsquare)
    disp('Finished localizing particles in experimental channel');
    toc;
    if Save_output == 1
        save([output_path,output_filename,'/','localized_particles_experimental'],'Localizations_raw_experiment');
    end
    
end

if Localize_Control == 1
    tic;
    % imaging parameters
impars.PixelSize=0.07; % um per pixel
impars.psf_scale=1.35; % PSF scaling
impars.NA=1.49; % NA of detection objective
impars.FrameRate= 1000; %secs %Not sure if this is actually in important input for localization, but don't want to mess with it. Nonesense number
impars.FrameSize= 1000; %secs

% localization parameters
locpars.wn=9; %detection box in pixels
locpars.errorRate= LocalizationError_Normalization; % error rate (10^-)
locpars.dfltnLoops= NumDeflationLoops; % number of deflation loops
locpars.minInt=0; %minimum intensity in counts
locpars.maxOptimIter= 50; % max number of iterations
locpars.termTol= -2; % termination tolerance
locpars.isRadiusTol=false; % use radius tolerance
locpars.radiusTol=50; % radius tolerance in percent
locpars.posTol= 1.5;%max position refinement
locpars.optim = [locpars.maxOptimIter,locpars.termTol,locpars.isRadiusTol,locpars.radiusTol,locpars.posTol];
locpars.isThreshLocPrec = false;
locpars.minLoc = 0;
locpars.maxLoc = inf;
locpars.isThreshSNR = false;
locpars.minSNR = 0;
locpars.maxSNR = inf;
locpars.isThreshDensity = false;
    disp('Localizing particles in normalization channel');
    impars.name = Experiment_condition_file;
    impars.wvlnth= EmissionWavelength_Control/1000; %emission wavelength in um
    impars.psfStd= impars.psf_scale*0.55*(impars.wvlnth)/impars.NA/1.17/impars.PixelSize/2; % PSF standard deviation in pixels
    Localizations_raw_normalization = localizeParticles_ASH_par(input_path,impars, locpars, Normalization_meta_stack);
    %data structured array: % ctrsX = x-coord of all localizations
    % ctrsY = y-coord of all localizations
    % ctrsN = number of localizations in a given
    % frame
    % signal = signal (obvious)
    % frame (In this case, subsquare)
    disp('Finished localizing particles in normalization channel');
    toc;
    if Save_output == 1
        save([output_path,output_filename,'/','localized_particles_normalization'],'Localizations_raw_normalization');
    end
    
end

clear 'All_masks' 'Experiment_meta_stack' 'Normalization_meta_stack' 
%end


%% Mask or Filter detections
tic;
if Localize_Experiment == 1
    disp('Separating detections into categories based on annotation for the experiment channel...');
    %MATRIX LAYOUT:
    % Column 1: Raw x-coordinates of detections
    % Column 2: Raw y-coordinates of detections
    % Column 3: ID of subsquare the detections came from
    % Column 4: X-coordinates in original image coordinate system
    % Column 5: Y-coordinates in original image coordinate system
    % Column 6: Signal of spot, as determined by MTT
    % Column 7: Normalized Spot Signal
    % Column 8: DAPI mask. 1 = inside nucleus, 0 = outside
    % Column 9: Nevus mask. 1 = inside, 0 = outside, 2 = melanocyte
    % annotation, 3 = Lymphocyte annotation, 4 = Fibroblast, 5 =
    % unannotated
    % Column 10: Melanocyte mask. 1 = inside, 0 = outside
    % Column 11: Lymphocyte mask. 1 = inside, 0 = outside
    % Column 12: Fibroblast mask. 1 = inside, 0 = outside
    
    Expt_localizations_matrix = zeros(length(Localizations_raw_experiment.ctrsX),12);
    Nuclear_loc_expt_matrix = [1,2,3,4,5,6,7,8,9,10,11,12];
    Nevus_loc_expt_matrix = [1,2,3,4,5,6,7,8,9,10,11,12];
    Lymphocyte_loc_expt_matrix = [1,2,3,4,5,6,7,8,9,10,11,12];
    Melanocyte_loc_expt_matrix = [1,2,3,4,5,6,7,8,9,10,11,12];
    Fibroblast_loc_expt_matrix = [1,2,3,4,5,6,7,8,9,10,11,12];
    for line=1:length(Localizations_raw_experiment.ctrsX)
        if Localizations_raw_experiment.signal(line) > Detection_threshold
            Expt_localizations_matrix(line,1) = Localizations_raw_experiment.ctrsX(line);
            Expt_localizations_matrix(line,2) = Localizations_raw_experiment.ctrsY(line);
            Expt_localizations_matrix(line,3) = Localizations_raw_experiment.frame(line);
            %Column 4: Localization x-coord with respect to larger image
            Expt_localizations_matrix(line,4) = (Localizations_raw_experiment.ctrsX(line))+((subsquare_reference_matrix(Localizations_raw_experiment.frame(line),2)-1)*field_detect_side_size);
            %Column 5: Localization y-coord with respect to larger image
            Expt_localizations_matrix(line,5) = (Localizations_raw_experiment.ctrsY(line))+((subsquare_reference_matrix(Localizations_raw_experiment.frame(line),1)-1)*field_detect_side_size);
            Expt_localizations_matrix(line,6) = Localizations_raw_experiment.signal(line);
            Expt_localizations_matrix(line,7) = 0;
            %%%%%%NOTE: ALL MASKS ARE IN IMAGE COORDINATES, WHICH DIFFER FROM
            %%%%%%CARTESIAN COORDINATES AND ARE MIRRORED ALONG Y=-X. HAVE TO
            %%%%%%TAKE INTO ACCOUNT WHEN MATCHING A DETECTING WITH A MASK
            if dapi_binary(round(Expt_localizations_matrix(line,5)),round(Expt_localizations_matrix(line,4))) == 1
                Expt_localizations_matrix(line,8) = 1;
                if Nevus_masks_all(round(Expt_localizations_matrix(line,5)),round(Expt_localizations_matrix(line,4))) == 1
                    Expt_localizations_matrix(line,9) = 1;
                elseif Melanocyte_masks_all(round(Expt_localizations_matrix(line,5)),round(Expt_localizations_matrix(line,4))) == 1
                    Expt_localizations_matrix(line,10) = 1;
                    Expt_localizations_matrix(line,9) = 2;
                elseif Lymphocyte_masks_all(round(Expt_localizations_matrix(line,5)),round(Expt_localizations_matrix(line,4))) == 1
                    Expt_localizations_matrix(line,11) = 1;
                    Expt_localizations_matrix(line,9) = 3;
                elseif Fibroblast_masks_all(round(Expt_localizations_matrix(line,5)),round(Expt_localizations_matrix(line,4))) == 1
                    Expt_localizations_matrix(line,12) = 1;
                    Expt_localizations_matrix(line,9) = 4;
                else
                    Expt_localizations_matrix(line,9) = 5;
                    Expt_localizations_matrix(line,10) = 2;
                    Expt_localizations_matrix(line,11) = 2;
                    Expt_localizations_matrix(line,12) = 2;
                end
            else
                Expt_localizations_matrix(line,7) = 2;
            end
        end
      
        %%%REMOVE DETECTIONS OUTSIDE OF A NUCLEUS
        if and(round(Expt_localizations_matrix(line,5)),round(Expt_localizations_matrix(line,4))) > 0
            if dapi_binary(round(Expt_localizations_matrix(line,5)),round(Expt_localizations_matrix(line,4))) == 1
                if Nuclear_loc_expt_matrix(1,:) == [1,2,3,4,5,6,7,8,9,10,11,12];
                    Nuclear_loc_expt_matrix = Expt_localizations_matrix(line,:);
                else
                    Nuclear_loc_expt_matrix = vertcat(Nuclear_loc_expt_matrix, Expt_localizations_matrix(line,:));
                end
            end
        end
    end
    

    
    if Save_output == 1
        save([output_path,output_filename,'/','localized_particles_experiment'],'Expt_localizations_matrix','Nuclear_loc_expt_matrix',...
            'Nevus_loc_expt_matrix','Melanocyte_loc_expt_matrix','Lymphocyte_loc_expt_matrix','Fibroblast_loc_expt_matrix');
        csvwrite([output_path,output_filename,'/','localized_particles_experiment.csv'],Nuclear_loc_expt_matrix);
        dlmwrite([output_path,output_filename,'/','localized_particles_experiment.txt'],Nuclear_loc_expt_matrix,'delimiter','\t');
    end
end

toc;

tic;
if Localize_Control == 1
    disp('Separating detections into categories based on annotation for the experiment channel...');
    %MATRIX LAYOUT:
    % Column 1: Raw x-coordinates of detections
    % Column 2: Raw y-coordinates of detections
    % Column 3: ID of subsquare the detections came from
    % Column 4: X-coordinates in original image coordinate system
    % Column 5: Y-coordinates in original image coordinate system
    % Column 6: Signal of spot, as determined by MTT
    % Column 7: DAPI mask. 1 = inside nucleus, 0 = outside
    % Column 8: Nevus mask. 1 = inside, 0 = outside
    % Column 9: Melanocyte mask. 1 = inside, 0 = outside
    % Column 10: Lymphocyte mask. 1 = inside, 0 = outside
    % Column 11: Fibroblast mask. 1 = inside, 0 = outside
    
    Normalization_localizations_matrix = zeros(length(Localizations_raw_normalization.ctrsX),11);
    Nuclear_loc_norm_matrix = [1,2,3,4,5,6,7,8,9,10,11];
    Nevus_loc_norm_matrix = [1,2,3,4,5,6,7,8,9,10,11];
    Lymphocyte_loc_norm_matrix = [1,2,3,4,5,6,7,8,9,10,11];
    Melanocyte_loc_norm_matrix = [1,2,3,4,5,6,7,8,9,10,11];
    Fibroblast_loc_norm_matrix = [1,2,3,4,5,6,7,8,9,10,11];
    for line=1:length(Localizations_raw_normalization.ctrsX)
        if Localizations_raw_normalization.signal(line) > Detection_threshold
            Normalization_localizations_matrix(line,1) = Localizations_raw_normalization.ctrsX(line);
            Normalization_localizations_matrix(line,2) = Localizations_raw_normalization.ctrsY(line);
            Normalization_localizations_matrix(line,3) = Localizations_raw_normalization.frame(line);
            %Column 4: Localization x-coord with respect to larger image
            Normalization_localizations_matrix(line,4) = (Localizations_raw_normalization.ctrsX(line))+((subsquare_reference_matrix(Localizations_raw_normalization.frame(line),2)-1)*field_detect_side_size);
            %Column 5: Localization y-coord with respect to larger image
            Normalization_localizations_matrix(line,5) = (Localizations_raw_normalization.ctrsY(line))+((subsquare_reference_matrix(Localizations_raw_normalization.frame(line),1)-1)*field_detect_side_size);
            Normalization_localizations_matrix(line,6) = Localizations_raw_normalization.signal(line);
            %%%%%%NOTE: ALL MASKS ARE IN IMAGE COORDINATES, WHICH DIFFER FROM
            %%%%%%CARTESIAN COORDINATES AND ARE MIRRORED ALONG Y=-X. HAVE TO
            %%%%%%TAKE INTO ACCOUNT WHEN MATCHING A DETECTING WITH A MASK
            if dapi_binary(round(Normalization_localizations_matrix(line,5)),round(Normalization_localizations_matrix(line,4))) == 1
                Normalization_localizations_matrix(line,7) = 1;
                if Nevus_masks_all(round(Normalization_localizations_matrix(line,5)),round(Normalization_localizations_matrix(line,4))) == 1
                    Normalization_localizations_matrix(line,8) = 1;
                elseif Melanocyte_masks_all(round(Normalization_localizations_matrix(line,5)),round(Normalization_localizations_matrix(line,4))) == 1
                    Normalization_localizations_matrix(line,9) = 1;
                    Normalization_localizations_matrix(line,8) = 2;
                elseif Lymphocyte_masks_all(round(Normalization_localizations_matrix(line,5)),round(Normalization_localizations_matrix(line,4))) == 1
                    Normalization_localizations_matrix(line,10) = 1;
                    Normalization_localizations_matrix(line,8) = 3;
                elseif Fibroblast_masks_all(round(Normalization_localizations_matrix(line,5)),round(Normalization_localizations_matrix(line,4))) == 1
                    Normalization_localizations_matrix(line,11) = 1;
                    Normalization_localizations_matrix(line,8) = 4;
                else
                    Normalization_localizations_matrix(line,8) = 5;
                    Normalization_localizations_matrix(line,9) = 2;
                    Normalization_localizations_matrix(line,10) = 2;
                    Normalization_localizations_matrix(line,11) = 2;
                end
            else
                Normalization_localizations_matrix(line,7) = 2;
            end
        end
        
      
        %%%SEPARATE DETECTIONS INTO DIFFERENT MATRICIES BASED ON ANNOTATION
        
        if and(round(Normalization_localizations_matrix(line,5)),round(Normalization_localizations_matrix(line,4))) > 0
            if dapi_binary(round(Normalization_localizations_matrix(line,5)),round(Normalization_localizations_matrix(line,4))) == 1
                if Nuclear_loc_norm_matrix(1,:) == [1,2,3,4,5,6,7,8,9,10,11];
                    Nuclear_loc_norm_matrix = Normalization_localizations_matrix(line,:);
                else
                    Nuclear_loc_norm_matrix = vertcat(Nuclear_loc_norm_matrix, Normalization_localizations_matrix(line,:));
                end
            end
        end
        
    end
    

    
    if Save_output == 1
        save([output_path,output_filename,'/','localized_particles_normalization'],'Normalization_localizations_matrix','Nuclear_loc_norm_matrix',...
            'Nevus_loc_norm_matrix','Melanocyte_loc_norm_matrix','Lymphocyte_loc_norm_matrix','Fibroblast_loc_norm_matrix');
        csvwrite([output_path,output_filename,'/','localized_particles_normalization.csv'],Nuclear_loc_norm_matrix);
        dlmwrite([output_path,output_filename,'/','localized_particles_normalization.txt'],Nuclear_loc_norm_matrix,'delimiter','\t');
    end
end
toc;
%%% Quantify and Normalize Spots

% NORMALIZATION BASED ON EUCLIDIAN NEAREST NEIGHBORS
tic;
disp('KNN algorithm to normalize the experimental channel to the control channel');
Experiment_localizations = [Nuclear_loc_expt_matrix(:,4), Nuclear_loc_expt_matrix(:,5)];
Normalization_localizations = [Nuclear_loc_norm_matrix(:,4), Nuclear_loc_norm_matrix(:,5)];

nearest_neighbor_matrix = knnsearch(Normalization_localizations,Experiment_localizations,'K',number_nearest_neighbor);
normalization_factors = zeros(1,number_nearest_neighbor);
for line = 1:length(Experiment_localizations)
    for index = 1:number_nearest_neighbor
        normalization_factors(index) = Nuclear_loc_norm_matrix(nearest_neighbor_matrix(line,index),6);
    end
    Nuclear_loc_expt_matrix(line,7) = Nuclear_loc_expt_matrix(line,6) / median(normalization_factors,2);
end
toc;

%% Sort detections into lists

%EXPERIMENTAL DATA
for line = 1:length(Nuclear_loc_expt_matrix)
    if Nuclear_loc_expt_matrix(line,9) == 1
        if Nevus_loc_expt_matrix(1,:) ~= [1,2,3,4,5,6,7,8,9,10,11,12];
            Nevus_loc_expt_matrix = vertcat(Nevus_loc_expt_matrix,Nuclear_loc_expt_matrix(line,:));
        else
            Nevus_loc_expt_matrix = Nuclear_loc_expt_matrix(line,:);
        end
    end
    
    if Nuclear_loc_expt_matrix(line,10) == 1
        if Melanocyte_loc_expt_matrix(1,:) ~= [1,2,3,4,5,6,7,8,9,10,11,12];
            Melanocyte_loc_expt_matrix = vertcat(Melanocyte_loc_expt_matrix,Nuclear_loc_expt_matrix(line,:));
        else
            Melanocyte_loc_expt_matrix = Nuclear_loc_expt_matrix(line,:);
        end
    end
    
    if Nuclear_loc_expt_matrix(line,11) == 1
        if Lymphocyte_loc_expt_matrix(1,:) ~= [1,2,3,4,5,6,7,8,9,10,11,12];
            Lymphocyte_loc_expt_matrix = vertcat(Lymphocyte_loc_expt_matrix,Nuclear_loc_expt_matrix(line,:));
        else
            Lymphocyte_loc_expt_matrix = Nuclear_loc_expt_matrix(line,:);
        end
    end
    
    if Nuclear_loc_expt_matrix(line,12) == 1
        if Fibroblast_loc_expt_matrix(1,:) ~= [1,2,3,4,5,6,7,8,9,10,11,12];
            Fibroblast_loc_expt_matrix = vertcat(Fibroblast_loc_expt_matrix,Nuclear_loc_expt_matrix(line,:));
        else
            Fibroblast_loc_expt_matrix = Nuclear_loc_expt_matrix(line,:);
        end
    end
end
            
% NORMALIZATION DATA
for line = 1:length(Nuclear_loc_norm_matrix)
    if Nuclear_loc_norm_matrix(line,8) == 1
        if Nevus_loc_norm_matrix(1,:) ~= [1,2,3,4,5,6,7,8,9,10,11];
            Nevus_loc_norm_matrix = vertcat(Nevus_loc_norm_matrix,Nuclear_loc_norm_matrix(line,:));
        else
            Nevus_loc_norm_matrix = Nuclear_loc_norm_matrix(line,:);
        end
    end
    
    if Nuclear_loc_norm_matrix(line,9) == 1
        if Melanocyte_loc_norm_matrix(1,:) ~= [1,2,3,4,5,6,7,8,9,10,11];
            Melanocyte_loc_norm_matrix = vertcat(Melanocyte_loc_norm_matrix,Nuclear_loc_norm_matrix(line,:));
        else
            Melanocyte_loc_norm_matrix = Nuclear_loc_norm_matrix(line,:);
        end
    end
    
    if Nuclear_loc_norm_matrix(line,10) == 1
        if Lymphocyte_loc_norm_matrix(1,:) ~= [1,2,3,4,5,6,7,8,9,10,11];
            Lymphocyte_loc_norm_matrix = vertcat(Lymphocyte_loc_norm_matrix,Nuclear_loc_norm_matrix(line,:));
        else
            Lymphocyte_loc_norm_matrix = Nuclear_loc_norm_matrix(line,:);
        end
    end

    
    
    if Nuclear_loc_norm_matrix(line,11) == 1
        if Fibroblast_loc_norm_matrix(1,:) ~= [1,2,3,4,5,6,7,8,9,10,11];
            Fibroblast_loc_norm_matrix = vertcat(Fibroblast_loc_norm_matrix,Nuclear_loc_norm_matrix(line,:));
        else
            Fibroblast_loc_norm_matrix = Nuclear_loc_norm_matrix(line,:);
        end
    end
end

if Save_output == 1
    save([output_path,output_filename,'/','localized_particles_experiment'],'Expt_localizations_matrix','Nuclear_loc_expt_matrix',...
        'Nevus_loc_expt_matrix','Melanocyte_loc_expt_matrix','Lymphocyte_loc_expt_matrix','Fibroblast_loc_expt_matrix')
    csvwrite([output_path,output_filename,'/','melanoma_localized_particles_experiment.csv'],Melanocyte_loc_expt_matrix);
    dlmwrite([output_path,output_filename,'/','melanoma_localized_particles_experiment.txt'],Melanocyte_loc_expt_matrix,'delimiter','\t');
    csvwrite([output_path,output_filename,'/','Nevus_localized_particles_experiment.csv'],Nevus_loc_expt_matrix);
    dlmwrite([output_path,output_filename,'/','Nevus_localized_particles_experiment.txt'],Nevus_loc_expt_matrix,'delimiter','\t');
    csvwrite([output_path,output_filename,'/','Nevus_localized_particles_experiment.csv'],Lymphocyte_loc_expt_matrix);
    dlmwrite([output_path,output_filename,'/','Nevus_localized_particles_experiment.txt'],Lymphocyte_loc_expt_matrix,'delimiter','\t');
    csvwrite([output_path,output_filename,'/','Fibro_localized_particles_experiment.csv'],Fibroblast_loc_expt_matrix);
    dlmwrite([output_path,output_filename,'/','Fibro_localized_particles_experiment.txt'],Fibroblast_loc_expt_matrix,'delimiter','\t');
    
    save([output_path,output_filename,'/','localized_particles_normalization'],'Normalization_localizations_matrix','Nuclear_loc_norm_matrix',...
        'Nevus_loc_norm_matrix','Melanocyte_loc_norm_matrix','Lymphocyte_loc_norm_matrix','Fibroblast_loc_norm_matrix');
end

