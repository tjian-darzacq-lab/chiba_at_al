% s1 = subplot(1,2,1);
% clims_exp = [400,6000];
% imagesc(original_image_experiment,clims_exp);
% colormap('parula');
% hold on
% scatter(Nuclear_loc_expt_matrix(:,4),Nuclear_loc_expt_matrix(:,5),50, 'go');
% hold off
%
% s2 = subplot(1,2,2);
% clims_cont = [100,2000];
% imagesc(original_image_control,clims_cont);
% colormap('parula');
% hold on
% scatter(Nuclear_loc_norm_matrix(:,4),Nuclear_loc_norm_matrix(:,5),50, 'go');
% hold off
%
% linkaxes([s1,s2], 'xy');



%%%% Bootstrapping %%%%
%%%randomly sample 1/2 of the Cy3 and Cy5 data sets. Compare mean, median,
%%%and overlap

Case_name = 'Case_4nvsm';
input_path = '/Users/Davidmcswiggen/Downloads/text files for bootstrap-selected/';
text_file_input = 'Case4nvsm.txt';
Data_set_1 = tdfread([input_path,text_file_input]);
melanoma_signal = zeros(length(Data_set_1.signal),1);
melanoma_normalized_signal = zeros(length(Data_set_1.signal),1);
nevus_signal = zeros(length(Data_set_1.signal),1);
nevus_normalized_signal = zeros(length(Data_set_1.signal),1);
radial_signal = [];

for entry = 1:length(Data_set_1.signal)
    if strcmp(num2str(Data_set_1.tissue(entry,:)),'Nevus   ')
        nevus_signal(entry) = Data_set_1.signal(entry);
        nevus_normalized_signal(entry) = Data_set_1.normalized_signal(entry);
    elseif strcmp(num2str(Data_set_1.tissue(entry,:)),'Melanoma')
        melanoma_signal(entry) = Data_set_1.signal(entry);
        melanoma_normalized_signal(entry) = Data_set_1.normalized_signal(entry);
    elseif strcmp(num2str(Data_set_1.tissue(entry,:)),'Radial  ')
        radial_signal(entry) = Data_set_1.signal(entry);
        radial_normalized_signal(entry) = Data_set_1.normalized_signal(entry);
    else
        continue;
    end
end

melanoma_signal = melanoma_signal(melanoma_signal~=0);
melanoma_normalized_signal = melanoma_normalized_signal(melanoma_normalized_signal~=0);
nevus_signal = nevus_signal(nevus_signal~=0);
nevus_normalized_signal = nevus_normalized_signal(nevus_normalized_signal~=0);
%Normalize all the signal so nevus median is 1
nevus_norm_factor = 1/median(nevus_normalized_signal,1);

melanoma_normalized_signal = melanoma_normalized_signal.*nevus_norm_factor;
nevus_normalized_signal = nevus_normalized_signal .* nevus_norm_factor;

if ~isempty(radial_signal)
    radial_signal = radial_signal(radial_signal~=0);
    radial_normalized_signal = radial_normalized_signal(radial_normalized_signal~=0);
    radial_normalized_signal = radial_normalized_signal .* nevus_norm_factor;
end


%bootstrap_sample_size = floor(min(length(melanoma_signal),length(nevus_signal))/2);
bootstrap_sample_size = 50;
n_bootstrap = 10000;

figure
hold on
for sample_size = 1:length(bootstrap_sample_size)
    resampled_melanoma = zeros(bootstrap_sample_size(1,sample_size),n_bootstrap);
    resampled_nevus = zeros(bootstrap_sample_size(1,sample_size),n_bootstrap);
    resampled_normalized_melanoma = zeros(bootstrap_sample_size(1,sample_size),n_bootstrap);
    resampled_normalized_nevus = zeros(bootstrap_sample_size(1,sample_size),n_bootstrap);
    
    for number = 1:n_bootstrap
        resampled_melanoma(:,number) = datasample(melanoma_signal(:,1),bootstrap_sample_size(1,sample_size),1,'Replace',false);
        resampled_normalized_melanoma(:,number) = datasample(melanoma_normalized_signal,bootstrap_sample_size(1,sample_size),1,'Replace',false);
        resampled_nevus(:,number) = datasample(nevus_signal,bootstrap_sample_size(1,sample_size),1,'Replace',false);
        resampled_normalized_nevus(:,number) = datasample(nevus_normalized_signal,bootstrap_sample_size(1,sample_size),1,'Replace',false);
    end
    
    median_resampled_melanoma = median(resampled_melanoma,1);
    median_resampled_nevus = median(resampled_nevus,1);
    
    median_normalized_resampled_melanoma = median(resampled_normalized_melanoma,1);
    median_normalized_resampled_nevus = median(resampled_normalized_nevus,1);
    difference_normalized = median_normalized_resampled_melanoma - median_normalized_resampled_nevus;
    fold_change_resampled_normalized = median_normalized_resampled_melanoma ./ median_normalized_resampled_nevus;
    subplot(2,4,sample_size)
    hold on
    histogram(median_normalized_resampled_melanoma)
    histogram(median_normalized_resampled_nevus)
    subplot(2,4,sample_size+4)
    histogram(fold_change_resampled_normalized,'Normalization','cdf')
end

dlmwrite([input_path,Case_name,'_nevus_values_renormalized.txt'], nevus_normalized_signal,'\t');
dlmwrite([input_path,Case_name,'_melanoma_values_renormalized.txt'], melanoma_normalized_signal,'\t');
dlmwrite([input_path,Case_name,'_fold_change_resample_',num2str(bootstrap_sample_size),'.txt'], fold_change_resampled_normalized,'\t');
if ~isempty(radial_signal)
    dlmwrite([input_path,Case_name,'_radial_values_renormalized.txt'], radial_normalized_signal,'\t');
end

% figure
% histogram(fold_change_normalized)
% xlabel('Fold change (Melanoma / Nevus signal)','FontSize',14)
% ylabel('Frequency','FontSize',14)
% text(1,2000,['Mean fold change: ', num2str(mean(fold_change_normalized))],'FontSize',14)
% text(1,1800,['95% Confidence interval: ', num2str(1.97*std(fold_change_normalized,1))],'FontSize',14)

