Chiba, Lorbeer, et. al.

David McSwiggen, September 2016

Code to detect and quantify spots from a single image plane for large
images, such as those from the StellarVision microscope
Questions about this code should be directed to dmcswiggen@berkeley.edu

The puropse of this code is to detect spots in DNA or RNA FISH images
using the MTT fitting algorithm as the basis for detection. Two image
files -- in this case Telomere and Centromere -- are analyzed separately
with one acting as an internal control for the other. A third image file
of a DAPI image (or similar), is used to identify cell nuclei and to ensure
that detections are bona fide spots rather than spurrious detections
stemming from the SAO reconstruction.

A user should specify an input path where the image files are held in the
'input_path', and identify the names of individual files, and then
specify where the processed files should be saved.

This code allows for the user to identify regions of the image containing
different cell types, and will later sort all of the detected spots into
one of these categories.

The output of this script is a list containing the coordinates,
intensity, and intensity after normalization of each detected spot. The
normalization uses the knnsearch function to identify n nearest spots in 
the internal control and takes the ratio of their median to each spot in
the experiment channel. For Chiba, Lorbeer et. al. 2017, we used n = 5, but there
is no reason a priori that a different value could not be used.